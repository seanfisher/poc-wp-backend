<?php
/**
 *
 */

class EXCL_API_07_Components extends WP_JSON_CustomPostType {
    protected $api_version = "v0.7";
    protected $base = '/excl/components';
    protected $type = 'component';

        public function __construct($server) {
        parent::__construct($server);
        $this->base = '/' . $this->api_version . $this->base;
    }

    public function register_routes( $routes ) {
        $routes = parent::register_routes( $routes );
        // $routes = parent::register_revision_routes( $routes );
        // $routes = parent::register_comment_routes( $routes );

        $routes[$this->base] = array(
            array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
            array( array( $this, 'new_post'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
        );
        $routes[$this->base . '/(?P<id>\d+)'] = array(
            array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
            array( array( $this, 'edit_post'), WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ),
            array( array( $this, 'delete_post'), WP_JSON_Server::DELETABLE ),
        );

        return $routes;
    }

    public function get_posts() {
        $raw_posts = parent::get_posts()->data;
        // die(var_dump($raw_posts));
        foreach($raw_posts as &$raw_post) {
            $raw_post = $this->compile_post($raw_post);
        }
        unset($raw_post);
        return $raw_posts;
    }

    public function get_post($id) {
        $post = parent::get_post($id)->data;
        return $this->compile_post($post);
    }

    /** Takes a standard post object and gets all of our custom data
     *  and formats it for the API
     */
    public function compile_post($post) {
        // Get custom fields
        $custom_fields = get_post_custom($post['ID']);
        $custom_keys = array_keys($custom_fields);
       
        die(var_dump($custom_keys));
        return $post;
    }

    // public function new_post() {
    //     return array('post' => 'true');
    // }
    // public function edit_post() {
    //     return array('post' => 'true');
    // }
    // public function delete_post() {
    //     return array('post' => 'true');
    // }
}