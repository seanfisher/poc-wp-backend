<?php
/**
 *
 */

class EXCL_API_V07 {

    function api_init( $server ) {
        $route_file_list = array('components', 'museums');
        add_routes($server, $route_file_list);
    }
}

function add_routes($server, $route_file_list) {
    $args = array($server);
    foreach ($route_file_list as $route_file_name) {
        require_once dirname( __FILE__ ) . '/'.$route_file_name.'.php';
        $reflectionClass = new ReflectionClass('EXCL_API_07_'.ucfirst($route_file_name));
        $module = $reflectionClass->newInstanceArgs($args);
        add_filter( 'json_endpoints', array( $module, 'register_routes' ) );
    }

}


