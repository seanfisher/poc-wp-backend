<?php
/**
 *
 */

class EXCL_API_V05 {

    function api_init( $server ) {
        require_once dirname( __FILE__ ) . '/components.php';
        $components = new EXCL_API_05_Components( $server );
        add_filter( 'json_endpoints', array( $components, 'register_routes' ) );
    }

}


