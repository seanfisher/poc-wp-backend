<?php
/**
 *
 */

class EXCL_API_05_Components extends WP_JSON_CustomPostType {
    protected $api_version;
    protected $custom_prefix = "wpcf-";
    protected $base = 'excl/components';
    protected $type = 'component';
    protected $component_whitelist = array('ID', 'title', 'description', 'type', 'categories', 'tags', 'component_creator', 'exhibit', 'museum', 'activities', 'big_questions', 'videos', 'images', 'thumbnails', 'surveys', 'rating', 'disp_rating');

    public function __construct($server, $api_version = "0.5") {
        parent::__construct($server);
        $this->api_version = 'v' . $api_version;
        $this->base = '/' . $this->api_version . '/' . $this->base;
    }

    public function register_routes( $routes ) {
        $routes = parent::register_routes( $routes );
        // $routes = parent::register_revision_routes( $routes );
        // $routes = parent::register_comment_routes( $routes );

        $routes[$this->base] = array(
            array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
            array( array( $this, 'new_post'), WP_JSON_Server::CREATABLE | WP_JSON_Server::ACCEPT_JSON ),
        );
        $routes[$this->base . '/(?P<id>\d+)'] = array(
            array( array( $this, 'get_post'), WP_JSON_Server::READABLE ),
            array( array( $this, 'edit_post'), WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ),
            array( array( $this, 'delete_post'), WP_JSON_Server::DELETABLE ),
        );

        return $routes;
    }

    public function get_posts() {
        $raw_posts = parent::get_posts()->data;
        foreach($raw_posts as &$raw_post) {
            $raw_post = $this->clean_post($raw_post);
        }
        unset($raw_post);

        $components['components'] = $raw_posts;
        return new WP_JSON_Response($this->wrap_json($components));
    }

    public function get_post($id) {
        $post = parent::get_post($id)->get_data();
        if ($post === null) {
            return new WP_Error( 'json_component_invalid_id', __( 'Invalid component ID.' ), array( 'status' => 404 ) );
        }

        $component['components'] = $this->clean_post($post);

        return new WP_JSON_Response($this->wrap_json($component));
    }

    /** Takes a standard post object and gets all of our custom data
     *  and formats it for the API
     */
    public function clean_post($post) {
        if ($post === null) {
            return new WP_Error( 'json_component_invalid_id', __( 'Invalid component ID.' ), array( 'status' => 404 ) );
        }
        // Get custom fields
        $custom_fields = get_post_custom($post['ID']);
        $custom_fields = $this->collapse_custom_fields($custom_fields);

        // Merge in the custom fields
        $compiled_post = array_merge($post, $custom_fields);

        // Get relationships
        $big_questions = $this->clean_big_questions($this->get_children('big-question', $post['ID']));
        $activities = $this->clean_activities($this->get_children('activity', $post['ID']));

        // Transform the names to our own schema
        $compiled_post['description'] = $compiled_post['content'];
        $compiled_post['activities'] = $activities;
        $compiled_post['big_questions'] = $big_questions;
        $compiled_post['exhibit'] = $compiled_post['exhibit'][0];

        // Eliminate all fields that are not in our white-list
        $compiled_post = array_intersect_key($compiled_post, array_fill_keys($this->component_whitelist, ""));

        return $compiled_post;
    }

    /** Takes a post's total custom fields and returns a cleaned-up version ready for JSONing */
    public function collapse_custom_fields($custom_fields) {
        $custom_keys = array_keys($custom_fields);
        $processed_custom_keys = array();
        $processed_custom_fields = array(); // output

        // Clean up the custom fields to get us the keys for our custom fields
        foreach($custom_keys as $custom_key) {
            // If the custom field starts with our custom prefix
            if (substr($custom_key, 0, strlen($this->custom_prefix)) === $this->custom_prefix) {
                $processed_custom_keys[$custom_key] = substr($custom_key, strlen($this->custom_prefix));
            }
        }

        // Build the custom fields object we want
        foreach($processed_custom_keys as $oldKey => $newKey) {
            $processed_custom_fields[$newKey] = $custom_fields[$oldKey];
        }
        return $processed_custom_fields;
    }

    public function wrap_json($data, $error = "") {
        $json = array();
        if (is_string($error) && $error !== "") {
            $json['status'] = "error";
            $json['error'] = $error;
        } else {
            $json['status'] = "ok";
        }

        $json['data'] = $data;
        return $json;
    }

    public function clean_big_questions($questions_array) {
        $return_array = array();
        foreach($questions_array as $question) {
            $return_array[] = $this->clean_big_question($question);
        }
        return $return_array;
    }

    public function clean_big_question($question) {
        $return_question = array();
        $return_question['ID'] = $question->ID;
        $return_question['priority'] = $question->priority;
        $return_question['content'] = $question->post_title;
        return $return_question;
    }

    public function clean_activities($activities_array) {
        $return_array = array();
        foreach($activities_array as $activity) {
            $return_array[] = $this->clean_activity($activity);
        }
        return $return_array;
    }

    public function clean_activity($activity) {
        $return_activity = array();
        $return_activity['ID'] = $activity->ID;
        $return_activity['priority'] = $activity->priority;
        $return_activity['activity_title'] = $activity->post_title;
        $return_activity['activity_description'] = "Activity Description Test";
        return $return_activity;
    }

    /** Gets this component's children of the given type (in WP slug format) */
    public function get_children($type, $component_id) {
        $args = array('post_type' => $type, 'meta_query' => array(array('key' => '_wpcf_belongs_' . $this->type . '_id', 'value' => "$component_id")));
        $posts = get_posts($args);
        return $posts;
    }

    // public function new_post() {
    //     return array('post' => 'true');
    // }
    // public function edit_post() {
    //     return array('post' => 'true');
    // }
    // public function delete_post() {
    //     return array('post' => 'true');
    // }
}