<?php
/*
Plugin Name: ExCl_API
Plugin URI: 
Description: Contains a custom API
Version: 0.5
Author: Pariveda Solutions
Author URI: http://www.parivedasolutions.com
License: GPL2
*/


require_once dirname( __FILE__ ) . '/api_0.5/main.php';
require_once dirname( __FILE__ ) . '/api_0.7/main.php';
//require_once dirname( __FILE__ ) . '/api_1.0/main.php';

add_action('wp_json_server_before_serve', array('EXCL_API_V05', 'api_init'));
add_action('wp_json_server_before_serve', array('EXCL_API_V07', 'api_init'));
//add_action('wp_json_server_before_serve', array('EXCL_API_V10', 'api_init'));


